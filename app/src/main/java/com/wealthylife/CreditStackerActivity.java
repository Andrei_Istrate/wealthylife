package com.wealthylife;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;



public class CreditStackerActivity extends Activity {

    WebView myWebView;
    private final String URL_LINK = "https://wealthy-getwel.rhcloud.com/Credit_Stacker/Source/html/creds.jsp?gid=-1&uid=161";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.credit_stacker);
        myWebView = (WebView)findViewById(R.id.myWebView);
        myWebView.loadUrl(URL_LINK);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
    }
}
