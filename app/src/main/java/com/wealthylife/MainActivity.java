package com.wealthylife;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


/**
 * Created by AndroidIntern on 24.05.2016.
 */


public class MainActivity extends Activity {


     Handler handle = new Handler(){

         @Override
         public void handleMessage(Message msg) {
             Intent intent = new Intent(MainActivity.this,CreditStackerActivity.class);
             startActivity(intent);
             finish();
         }
     };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handle.sendEmptyMessageDelayed(0,3000);


    }
}
